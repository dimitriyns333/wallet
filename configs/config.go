package configs

import (
	"encoding/json"
	"log/slog"
	"os"
)

type Settings struct {
	HttpPort                 int         `json:"HttpPort"`
	GrpcPort                 int         `json:"GrpcPort"`
	Redis                    Redis       `json:"Redis"`
	ShutdownTimeoutSec       int         `json:"ShutdownTimeoutSec"`
	BaseTimeoutCancelSec     int         `json:"BaseTimeoutCancelSec"`
	WrapperDefaultTimeoutSec int         `json:"WrapperDefaultTimeoutSec"`
	GrpcClients              GrpcClients `json:"GrpcClients"`
}

type Redis struct {
	Addr            string `json:"Addr"`
	Password        string `json:"Password"`
	Db              int    `json:"Db"`
	MaxWatchRetries int    `json:"MaxWatchRetries"`
}

type GrpcClients struct {
	WalletClient      GrpcClient `json:"WalletClient"`
	LeaderboardClient GrpcClient `json:"LeaderboardClient"`
}

type GrpcClient struct {
	Addr string `json:"Addr"`
}

var settings Settings

func init() {
	bytes, err := os.ReadFile("config.json")
	if err != nil {
		slog.Error("[config] read", err)
		panic(err)
	}

	if err = json.Unmarshal(bytes, &settings); err != nil {
		slog.Error("[config] unmarshal", err)
		panic(err)
	}
}

func GetConfig() Settings {
	return settings
}
