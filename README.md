# Wallet app

## Tools
### Mockery

Mocks generation (powershell): ```docker run -v ${PWD}:/src -w /src vektra/mockery --all```

Mocks generation (bash): ```docker run -v "$PWD":/src -w /src vektra/mockery --all```

### Code generation

Code generation for rest api spec and for gRPC: ```go generate ./...```

### RUN

```docker-compose up```

### Tests

```go test ./...```

### Docs

```http://localhost:8080/api/rest_api_docs```

### Rest Api

There are 2 kinds of api: rest and gRPC.

To use wallet api you need to get jwt token, for this call:

```http://localhost:8080/api/fake_jwt/:userId```

Then api will be available via Authorization header.

Wallet api:

```http://localhost:8080/api/rest_api_docs#operation/Deposit```

```http://localhost:8080/api/rest_api_docs#operation/Withdraw```

```http://localhost:8080/api/rest_api_docs#operation/Balance```

### gRPC

gRPC api available via 8081 port.

Stream code example located here: ```pkg/services/leaderboard_service/service.go```

Also available via rest api ```http://localhost:8080/api/rest_api_docs#operation/Leaderboards```

Unary RPC example: ```pkg/usecases/wallet_usecase/usecase.go:50```

Or via rest: ```http://localhost:8080/api/rest_api_docs#operation/BalanceGrpc```
