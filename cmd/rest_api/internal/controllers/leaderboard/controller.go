package leaderboard

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wallet/cmd/rest_api/internal/api_gen"
	"wallet/pkg/domains/leaderboard_domain"
)

type ControllerLeaderboard struct {
	usecase leaderboard_domain.Usecase
}

func New(usecase leaderboard_domain.Usecase) *ControllerLeaderboard {
	return &ControllerLeaderboard{usecase: usecase}
}

func (c *ControllerLeaderboard) Leaderboards(ctx *gin.Context) {
	data, err := c.usecase.GetLeaderboards(ctx)
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	ctx.JSON(http.StatusOK, api_gen.BaseResponse{"data": api_gen.LeaderboardsResponse{List: data.List}})
}
