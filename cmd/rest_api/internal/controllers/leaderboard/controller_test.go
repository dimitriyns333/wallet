package leaderboard

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"wallet/cmd/rest_api/internal/api_gen"
	"wallet/pkg/domains/leaderboard_domain"
	"wallet/pkg/domains/leaderboard_domain/mocks/leaderboard_domain_usecase_mock"
)

func TestMain(m *testing.M) {
	gin.SetMode(gin.ReleaseMode)

	os.Exit(m.Run())
}

func initMocks() (*leaderboard_domain_usecase_mock.Mock, *ControllerLeaderboard, *gin.Engine) {
	leaderboardUsecaseMock := new(leaderboard_domain_usecase_mock.Mock)
	leaderboardController := New(leaderboardUsecaseMock)
	ginEngine := gin.New()

	ginEngine.Use(func(c *gin.Context) {
		c.Next()
		detectedErrors := c.Errors.ByType(gin.ErrorTypeAny)

		if len(detectedErrors) > 0 {
			err := detectedErrors[0].Err
			c.String(http.StatusInternalServerError, err.Error())
			c.Abort()
		}
	})

	return leaderboardUsecaseMock, leaderboardController, ginEngine
}

func TestControllerWallet_Balance(t *testing.T) {
	path := "/leaderboards"

	t.Run("success", func(t *testing.T) {
		mockResponse := &leaderboard_domain.LeaderboardsResponse{List: []float32{1, 2}}

		leaderboardUsecaseMock, leaderboardController, ginEngine := initMocks()

		leaderboardUsecaseMock.EXPECT().GetLeaderboards(mock.Anything).Return(mockResponse, nil).Once()

		rec := httptest.NewRecorder()

		ginEngine.GET(path, leaderboardController.Leaderboards)

		req := httptest.NewRequest(http.MethodGet, path, nil)

		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusOK, rec.Code)

		body, err := json.Marshal(&api_gen.BaseResponse{"data": api_gen.LeaderboardsResponse{List: mockResponse.List}})
		a.NoError(err)

		a.Equal(body, rec.Body.Bytes())

		leaderboardUsecaseMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		leaderboardUsecaseMock, leaderboardController, ginEngine := initMocks()

		customErr := errors.New("unexpected")
		leaderboardUsecaseMock.EXPECT().GetLeaderboards(mock.Anything).Return(nil, customErr)

		rec := httptest.NewRecorder()

		ginEngine.GET(path, leaderboardController.Leaderboards)

		req := httptest.NewRequest(http.MethodGet, path, nil)
		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusInternalServerError, rec.Code)

		a.Equal([]byte(customErr.Error()), rec.Body.Bytes())

		leaderboardUsecaseMock.AssertExpectations(t)
	})
}
