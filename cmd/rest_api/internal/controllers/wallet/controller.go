package wallet

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/cmd/rest_api/internal/api_gen"
	"wallet/pkg/common_errors"
	"wallet/pkg/domains/wallet_domain"
	"wallet/pkg/jwt"
)

type ControllerWallet struct {
	usecase wallet_domain.Usecase
}

func New(usecase wallet_domain.Usecase) *ControllerWallet {
	return &ControllerWallet{usecase: usecase}
}

func (c *ControllerWallet) Deposit(ctx *gin.Context) {
	var req api_gen.DepositRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		_ = ctx.Error(fmt.Errorf("%w: %w", common_errors.BadRequestAppErr, err))
		return
	}

	data, err := c.usecase.Deposit(ctx, ctx.GetString(jwt.UserIdHeader),
		wallet_domain.DepositRequest{Amount: req.Amount})
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	ctx.JSON(http.StatusOK, api_gen.BaseResponse{"data": api_gen.DepositResponse{Balance: data.Balance}})
}

func (c *ControllerWallet) Withdraw(ctx *gin.Context) {
	var req api_gen.WithdrawRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		_ = ctx.Error(fmt.Errorf("%w: %w", common_errors.BadRequestAppErr, err))
		return
	}

	data, err := c.usecase.Withdraw(ctx, ctx.GetString(jwt.UserIdHeader),
		wallet_domain.WithdrawRequest{Amount: req.Amount})
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	ctx.JSON(http.StatusOK, api_gen.BaseResponse{"data": api_gen.WithdrawResponse{Balance: data.Balance}})
}

func (c *ControllerWallet) Balance(ctx *gin.Context) {
	data, err := c.usecase.Balance(ctx, ctx.GetString(jwt.UserIdHeader))
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	ctx.JSON(http.StatusOK, api_gen.BaseResponse{"data": api_gen.BalanceResponse{Balance: data.Balance}})
}

func (c *ControllerWallet) BalanceGrpc(ctx *gin.Context) {
	data, err := c.usecase.BalanceGrpc(ctx, &wallet.GetBalanceRequest{UserId: ctx.GetString(jwt.UserIdHeader)})
	if err != nil {
		_ = ctx.Error(err)
		return
	}

	ctx.JSON(http.StatusOK, api_gen.BaseResponse{"data": api_gen.BalanceResponse{Balance: float64(data.Balance)}})
}
