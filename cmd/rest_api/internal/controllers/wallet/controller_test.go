package wallet

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/cmd/rest_api/internal/api_gen"
	"wallet/pkg/domains/wallet_domain"
	"wallet/pkg/domains/wallet_domain/mocks/wallet_domain_usecase_mock"
	"wallet/pkg/jwt"
)

var userId string
var amount float64

func TestMain(m *testing.M) {
	userId = "1"
	amount = 10

	gin.SetMode(gin.ReleaseMode)

	os.Exit(m.Run())
}

func initMocks() (*wallet_domain_usecase_mock.Mock, *ControllerWallet, *gin.Engine) {
	walletUsecaseMock := new(wallet_domain_usecase_mock.Mock)
	walletController := New(walletUsecaseMock)
	ginEngine := gin.New()

	ginEngine.Use(func(c *gin.Context) {
		c.Set(jwt.UserIdHeader, userId)
		c.Next()
		detectedErrors := c.Errors.ByType(gin.ErrorTypeAny)

		if len(detectedErrors) > 0 {
			err := detectedErrors[0].Err
			c.String(http.StatusInternalServerError, err.Error())
			c.Abort()
		}
	})

	return walletUsecaseMock, walletController, ginEngine
}

func TestControllerWallet_Deposit(t *testing.T) {
	path := "/deposit"
	mockRequest := api_gen.DepositRequest{Amount: amount}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.DepositResponse{Balance: amount}

		walletUsecaseMock, walletController, ginEngine := initMocks()

		walletUsecaseMock.EXPECT().Deposit(mock.Anything, userId,
			wallet_domain.DepositRequest{Amount: mockRequest.Amount}).Return(mockResponse, nil).Once()

		rec := httptest.NewRecorder()

		ginEngine.POST(path, walletController.Deposit)

		var mockRequestBuf bytes.Buffer
		err := json.NewEncoder(&mockRequestBuf).Encode(mockRequest)
		assert.NoError(t, err)

		req := httptest.NewRequest(http.MethodPost, path, &mockRequestBuf)

		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusOK, rec.Code)

		var body []byte
		body, err = json.Marshal(&api_gen.BaseResponse{"data": mockResponse})
		a.NoError(err)

		a.Equal(body, rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletUsecaseMock, walletController, ginEngine := initMocks()

		customErr := errors.New("unexpected")
		walletUsecaseMock.EXPECT().Deposit(mock.Anything, userId,
			wallet_domain.DepositRequest{Amount: mockRequest.Amount}).Return(nil, customErr)

		rec := httptest.NewRecorder()

		ginEngine.POST(path, walletController.Deposit)

		var mockRequestBuf bytes.Buffer
		err := json.NewEncoder(&mockRequestBuf).Encode(mockRequest)
		assert.NoError(t, err)

		req := httptest.NewRequest(http.MethodPost, path, &mockRequestBuf)
		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusInternalServerError, rec.Code)

		a.Equal([]byte(customErr.Error()), rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})
}

func TestControllerWallet_Withdraw(t *testing.T) {
	path := "/withdraw"
	mockRequest := api_gen.WithdrawRequest{Amount: amount}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.WithdrawResponse{Balance: amount}

		walletUsecaseMock, walletController, ginEngine := initMocks()

		walletUsecaseMock.EXPECT().Withdraw(mock.Anything, userId,
			wallet_domain.WithdrawRequest{Amount: mockRequest.Amount}).Return(mockResponse, nil).Once()

		rec := httptest.NewRecorder()

		ginEngine.POST(path, walletController.Withdraw)

		var mockRequestBuf bytes.Buffer
		err := json.NewEncoder(&mockRequestBuf).Encode(mockRequest)
		assert.NoError(t, err)

		req := httptest.NewRequest(http.MethodPost, path, &mockRequestBuf)

		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusOK, rec.Code)

		var body []byte
		body, err = json.Marshal(&api_gen.BaseResponse{"data": mockResponse})
		a.NoError(err)

		a.Equal(body, rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletUsecaseMock, walletController, ginEngine := initMocks()

		customErr := errors.New("unexpected")
		walletUsecaseMock.EXPECT().Withdraw(mock.Anything, userId,
			wallet_domain.WithdrawRequest{Amount: mockRequest.Amount}).Return(nil, customErr)

		rec := httptest.NewRecorder()

		ginEngine.POST(path, walletController.Withdraw)

		var mockRequestBuf bytes.Buffer
		err := json.NewEncoder(&mockRequestBuf).Encode(mockRequest)
		assert.NoError(t, err)

		req := httptest.NewRequest(http.MethodPost, path, &mockRequestBuf)
		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusInternalServerError, rec.Code)

		a.Equal([]byte(customErr.Error()), rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})
}

func TestControllerWallet_Balance(t *testing.T) {
	path := "/balance"

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.BalanceResponse{Balance: amount}

		walletUsecaseMock, walletController, ginEngine := initMocks()

		walletUsecaseMock.EXPECT().Balance(mock.Anything, userId).Return(mockResponse, nil).Once()

		rec := httptest.NewRecorder()

		ginEngine.GET(path, walletController.Balance)

		req := httptest.NewRequest(http.MethodGet, path, nil)

		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusOK, rec.Code)

		body, err := json.Marshal(&api_gen.BaseResponse{"data": mockResponse})
		a.NoError(err)

		a.Equal(body, rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletUsecaseMock, walletController, ginEngine := initMocks()

		customErr := errors.New("unexpected")
		walletUsecaseMock.EXPECT().Balance(mock.Anything, userId).Return(nil, customErr)

		rec := httptest.NewRecorder()

		ginEngine.GET(path, walletController.Balance)

		req := httptest.NewRequest(http.MethodGet, path, nil)
		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusInternalServerError, rec.Code)

		a.Equal([]byte(customErr.Error()), rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})
}

func TestControllerWallet_BalanceGrpc(t *testing.T) {
	path := "/balance_grpc"
	mockRequest := wallet.GetBalanceRequest{UserId: userId}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet.GetBalanceResponse{Balance: float32(amount)}

		walletUsecaseMock, walletController, ginEngine := initMocks()

		walletUsecaseMock.EXPECT().BalanceGrpc(mock.Anything, &mockRequest).Return(mockResponse, nil).Once()

		rec := httptest.NewRecorder()

		ginEngine.GET(path, walletController.BalanceGrpc)

		req := httptest.NewRequest(http.MethodGet, path, nil)

		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusOK, rec.Code)

		body, err := json.Marshal(&api_gen.BaseResponse{"data": wallet_domain.BalanceResponse{Balance: amount}})
		a.NoError(err)

		a.Equal(body, rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletUsecaseMock, walletController, ginEngine := initMocks()

		customErr := errors.New("unexpected")
		walletUsecaseMock.EXPECT().BalanceGrpc(mock.Anything, &mockRequest).Return(nil, customErr)

		rec := httptest.NewRecorder()

		ginEngine.GET(path, walletController.BalanceGrpc)

		req := httptest.NewRequest(http.MethodGet, path, nil)
		ginEngine.ServeHTTP(rec, req)

		a := assert.New(t)

		a.Equal(http.StatusInternalServerError, rec.Code)

		a.Equal([]byte(customErr.Error()), rec.Body.Bytes())

		walletUsecaseMock.AssertExpectations(t)
	})
}
