package rest_api

import (
	"context"
	"errors"
	"fmt"
	"github.com/deepmap/oapi-codegen/pkg/gin-middleware"
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/gin-gonic/gin"
	"log/slog"
	"net"
	"net/http"
	"strings"
	"wallet/cmd/rest_api/internal/api_gen"
	"wallet/cmd/rest_api/internal/controllers/leaderboard"
	"wallet/cmd/rest_api/internal/controllers/wallet"
	"wallet/cmd/rest_api/redoc"
	"wallet/pkg/common_errors"
	"wallet/pkg/di"
	"wallet/pkg/jwt"
)

type Api struct {
	server *http.Server
}

type ServerRunChan struct {
	Started bool
	Err     error
}

func New(port int) *Api {
	swagger, err := api_gen.GetSwagger()
	if err != nil {
		slog.Error("[rest api]", "error loading swagger spec", err)
		panic(err)
	}

	var fa jwt.JWSValidator
	fa, err = jwt.NewFakeAuthenticator()
	if err != nil {
		slog.Error("[rest api]", "jwt validator error", err)
		panic(err)
	}

	swagger.Servers = []*openapi3.Server{{URL: "/api", Description: "Needed to match path"}}

	gin.SetMode(gin.ReleaseMode)
	ginEngine := gin.Default()

	swaggerBytes, err := swagger.MarshalJSON()
	if err != nil {
		slog.Error("[rest api] swagger marshal json", "error", err)
		panic(err)
	}

	docsPath := "/api/rest_api_docs"
	doc := redoc.Redoc{
		SpecUrl:     fmt.Sprintf("%v/openapi.json", docsPath),
		Title:       swagger.Info.Title,
		Description: swagger.Info.Description,
	}
	docHandler := doc.Handler()
	ginEngine.GET(doc.SpecUrl, func(c *gin.Context) {
		c.Data(http.StatusOK, "application/json", swaggerBytes)
	})
	ginEngine.GET(docsPath, docHandler)
	slog.Info(fmt.Sprintf("[rest api] swagger docs available at %v", docsPath))

	apiGroup := ginEngine.Group("/api")
	ginEngine.Use(middleware.OapiRequestValidator(swagger))

	usecases := di.GetUsecases()

	// init controllers
	controllers := struct {
		*wallet.ControllerWallet
		*leaderboard.ControllerLeaderboard
		*ControllerFakeJwt
	}{
		ControllerWallet:      wallet.New(usecases.Wallet()),
		ControllerLeaderboard: leaderboard.New(usecases.Leaderboard()),
		ControllerFakeJwt:     NewControllerFakeJwt(fa),
	}

	host := net.JoinHostPort("0.0.0.0", fmt.Sprint(port))

	errorHandler := func(c *gin.Context, err error, _ int) {
		var parsedError *common_errors.AppError
		if !errors.As(err, &parsedError) {
			parsedError = common_errors.InternalAppErr
		}

		statusCode := http.StatusInternalServerError

		// Put the error into response
		if strings.EqualFold(parsedError.Error(), common_errors.ItemNotFoundAppErr.Error()) {
			statusCode = http.StatusNotFound
		} else if strings.EqualFold(parsedError.Error(), common_errors.BadRequestAppErr.Error()) {
			statusCode = http.StatusBadRequest
		} else if strings.EqualFold(parsedError.Error(), common_errors.Unauthorized.Error()) {
			statusCode = http.StatusUnauthorized
		} else {
			slog.ErrorContext(c, "[rest api]", "internal error", err)
			parsedError = common_errors.InternalAppErr
			c.JSON(statusCode, api_gen.Error{Message: parsedError.Error()})
			c.Abort()
			return
		}

		errMessage := err.Error()
		if strings.HasPrefix(errMessage, parsedError.Error()) {
			errMessage = fmt.Sprintf("%v%v", parsedError.Error(), strings.TrimPrefix(errMessage, parsedError.Error()))
		}

		c.JSON(statusCode, api_gen.Error{Message: errMessage})
	}

	apiGroup.Use(func(c *gin.Context) {
		api_gen.MiddlewareFunc(middleware.OapiRequestValidatorWithOptions(swagger,
			&middleware.Options{
				Options: openapi3filter.Options{
					AuthenticationFunc: jwt.NewAuthenticator(fa),
				},
				ErrorHandler: func(c *gin.Context, message string, statusCode int) {
					if strings.Contains(message, "SecurityRequirementsError") {
						errorHandler(c, fmt.Errorf("%w: %v", common_errors.Unauthorized, message), statusCode)
						return
					}

					errorHandler(c, fmt.Errorf("%w: %v", common_errors.BadRequestAppErr, message), statusCode)
				},
			}))(c)
		c.Next()
	})

	apiGroup.Use(func(c *gin.Context) {
		c.Next()

		if len(c.Errors) > 0 {
			errorHandler(c, c.Errors[0], c.Writer.Status())
		}
	})

	api_gen.RegisterHandlersWithOptions(apiGroup, controllers, api_gen.GinServerOptions{
		ErrorHandler: errorHandler,
	})

	return &Api{
		server: &http.Server{
			Addr:    host,
			Handler: ginEngine,
		},
	}
}

type ControllerFakeJwt struct {
	validator jwt.JWSValidator
}

func NewControllerFakeJwt(validator jwt.JWSValidator) *ControllerFakeJwt {
	return &ControllerFakeJwt{validator: validator}
}

func (c *ControllerFakeJwt) FakeJwt(ctx *gin.Context, userId string) {
	readerJWS, err := c.validator.CreateJWSWithClaims(userId, []string{})
	if err != nil {
		_ = ctx.Error(err)
	}

	ctx.JSON(http.StatusOK, api_gen.FakeJwtResponse{Jwt: string(readerJWS)})
}

func (a *Api) Run() <-chan ServerRunChan {
	ch := make(chan ServerRunChan, 2)

	go func() {
		defer close(ch)
		ch <- ServerRunChan{Started: true}

		if err := a.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			ch <- ServerRunChan{
				Started: false,
				Err:     err,
			}

			return
		}

		ch <- ServerRunChan{Started: false}
	}()

	return ch
}

func (a *Api) Shutdown(ctx context.Context) error {
	return a.server.Shutdown(ctx)
}
