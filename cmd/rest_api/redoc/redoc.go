package redoc

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"net/http"
	"text/template"

	_ "embed"
)

type Redoc struct {
	SpecUrl     string
	Title       string
	Description string
}

//go:embed assets/index.html
var HTML string

//go:embed assets/redoc.standalone.js
var JavaScript string

func (r Redoc) Body() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	tpl, err := template.New("redoc").Parse(HTML)
	if err != nil {
		return nil, err
	}

	if err = tpl.Execute(buf, map[string]string{
		"body":        JavaScript,
		"title":       r.Title,
		"specUrl":     r.SpecUrl,
		"description": r.Description,
	}); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func (r Redoc) Handler() gin.HandlerFunc {
	data, err := r.Body()
	if err != nil {
		panic(err)
	}

	return func(ctx *gin.Context) {
		ctx.Data(http.StatusOK, "text/html", data)
	}
}
