package leaderboard_server

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"os"
	"testing"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
	"wallet/cmd/grpc_api/rpc_services/leaderboard/mocks/leaderboard_service_list_leaderboards_server_mock"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func initMocks() (*leaderboardServer, *leaderboard_service_list_leaderboards_server_mock.Mock) {
	listLeaderboardsServerMock := new(leaderboard_service_list_leaderboards_server_mock.Mock)
	leaderboardService := New().(*leaderboardServer)
	leaderboardService.timeout = 0

	return leaderboardService, listLeaderboardsServerMock
}

func TestLeaderboardServer_ListLeaderboards(t *testing.T) {
	mockRequest := leaderboard.ListLeaderboardsRequest{}

	t.Run("success", func(t *testing.T) {
		leaderboardService, listLeaderboardsServerMock := initMocks()

		listLeaderboardsServerMock.EXPECT().Send(mock.Anything).Return(nil).Times(3)

		err := leaderboardService.ListLeaderboards(&mockRequest, listLeaderboardsServerMock)

		a := assert.New(t)

		a.NoError(err)

		listLeaderboardsServerMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		customErr := errors.New("unexpected")

		leaderboardService, listLeaderboardsServerMock := initMocks()

		listLeaderboardsServerMock.EXPECT().Send(mock.Anything).Return(customErr).Once()

		err := leaderboardService.ListLeaderboards(&mockRequest, listLeaderboardsServerMock)

		a := assert.New(t)

		a.Error(err)
		a.ErrorIs(customErr, err)

		listLeaderboardsServerMock.AssertExpectations(t)
	})
}
