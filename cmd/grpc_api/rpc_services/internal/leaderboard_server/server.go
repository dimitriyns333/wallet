package leaderboard_server

import (
	"math/rand"
	"time"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
)

type leaderboardServer struct {
	leaderboard.UnimplementedServiceServer
	timeout time.Duration
}

func New() leaderboard.ServiceServer {
	return &leaderboardServer{
		timeout: 2 * time.Second,
	}
}

func (s *leaderboardServer) ListLeaderboards(_ *leaderboard.ListLeaderboardsRequest,
	stream leaderboard.Service_ListLeaderboardsServer) error {
	for i := 0; i < 3; i++ {
		if err := stream.Send(&leaderboard.ListLeaderboardsResponse{List: s.randFloats(0, 100, 10)}); err != nil {
			return err
		}
	}

	return nil
}

func (s *leaderboardServer) randFloats(min, max float32, n int) []float32 {
	res := make([]float32, n)
	for i := range res {
		res[i] = min + rand.Float32()*(max-min)
	}
	time.Sleep(s.timeout) // for test
	return res
}
