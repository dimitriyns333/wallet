package wallet_server

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"os"
	"testing"
	"wallet/cmd/grpc_api/internal/error_handler"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/pkg/domains/wallet_domain"
	"wallet/pkg/domains/wallet_domain/mocks/wallet_domain_usecase_mock"
)

var userId string
var amount float64

func TestMain(m *testing.M) {
	userId = "1"
	amount = 10

	os.Exit(m.Run())
}

func initMocks() (*wallet_domain_usecase_mock.Mock, wallet.ServiceServer) {
	walletUsecaseMock := new(wallet_domain_usecase_mock.Mock)
	walletService := New(walletUsecaseMock)

	return walletUsecaseMock, walletService
}

func TestWalletServer_GetBalance(t *testing.T) {
	mockRequest := wallet.GetBalanceRequest{UserId: userId}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.BalanceResponse{Balance: amount}

		walletUsecaseMock, walletService := initMocks()

		walletUsecaseMock.EXPECT().Balance(mock.Anything, userId).Return(mockResponse, nil).Once()

		resp, err := walletService.GetBalance(context.Background(), &mockRequest)

		a := assert.New(t)

		a.NoError(err)
		a.NotNil(resp)

		a.Equal(float64(resp.Balance), mockResponse.Balance)

		walletUsecaseMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		customErr := errors.New("unexpected")

		walletUsecaseMock, walletService := initMocks()

		walletUsecaseMock.EXPECT().Balance(mock.Anything, userId).Return(nil, customErr).Once()

		resp, err := walletService.GetBalance(context.Background(), &mockRequest)

		a := assert.New(t)

		a.Nil(resp)
		a.Error(err)
		a.ErrorIs(error_handler.HandleResponseErr(context.Background(), customErr).Err(), err)

		walletUsecaseMock.AssertExpectations(t)
	})
}
