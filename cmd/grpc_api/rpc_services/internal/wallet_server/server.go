package wallet_server

import (
	"context"
	"wallet/cmd/grpc_api/internal/error_handler"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/pkg/domains/wallet_domain"
)

type walletServer struct {
	wallet.UnimplementedServiceServer
	usecase wallet_domain.Usecase
}

func New(usecase wallet_domain.Usecase) wallet.ServiceServer {
	return &walletServer{usecase: usecase}
}

func (s *walletServer) GetBalance(ctx context.Context, req *wallet.GetBalanceRequest) (*wallet.GetBalanceResponse, error) {
	resp, err := s.usecase.Balance(ctx, req.UserId)
	if err != nil {
		return nil, error_handler.HandleResponseErr(ctx, err).Err()
	}

	return &wallet.GetBalanceResponse{Balance: float32(resp.Balance)}, nil
}
