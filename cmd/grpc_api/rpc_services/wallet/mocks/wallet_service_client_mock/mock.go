// Code generated by mockery v2.32.4. DO NOT EDIT.

package wallet_service_client_mock

import (
	context "context"

	grpc "google.golang.org/grpc"

	mock "github.com/stretchr/testify/mock"

	wallet "wallet/cmd/grpc_api/rpc_services/wallet"
)

// Mock is an autogenerated mock type for the ServiceClient type
type Mock struct {
	mock.Mock
}

type Mock_Expecter struct {
	mock *mock.Mock
}

func (_m *Mock) EXPECT() *Mock_Expecter {
	return &Mock_Expecter{mock: &_m.Mock}
}

// GetBalance provides a mock function with given fields: ctx, in, opts
func (_m *Mock) GetBalance(ctx context.Context, in *wallet.GetBalanceRequest, opts ...grpc.CallOption) (*wallet.GetBalanceResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	var r0 *wallet.GetBalanceResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *wallet.GetBalanceRequest, ...grpc.CallOption) (*wallet.GetBalanceResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *wallet.GetBalanceRequest, ...grpc.CallOption) *wallet.GetBalanceResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*wallet.GetBalanceResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *wallet.GetBalanceRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Mock_GetBalance_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetBalance'
type Mock_GetBalance_Call struct {
	*mock.Call
}

// GetBalance is a helper method to define mock.On call
//   - ctx context.Context
//   - in *wallet.GetBalanceRequest
//   - opts ...grpc.CallOption
func (_e *Mock_Expecter) GetBalance(ctx interface{}, in interface{}, opts ...interface{}) *Mock_GetBalance_Call {
	return &Mock_GetBalance_Call{Call: _e.mock.On("GetBalance",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *Mock_GetBalance_Call) Run(run func(ctx context.Context, in *wallet.GetBalanceRequest, opts ...grpc.CallOption)) *Mock_GetBalance_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*wallet.GetBalanceRequest), variadicArgs...)
	})
	return _c
}

func (_c *Mock_GetBalance_Call) Return(_a0 *wallet.GetBalanceResponse, _a1 error) *Mock_GetBalance_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Mock_GetBalance_Call) RunAndReturn(run func(context.Context, *wallet.GetBalanceRequest, ...grpc.CallOption) (*wallet.GetBalanceResponse, error)) *Mock_GetBalance_Call {
	_c.Call.Return(run)
	return _c
}

// NewMock creates a new instance of Mock. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMock(t interface {
	mock.TestingT
	Cleanup(func())
}) *Mock {
	mock := &Mock{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
