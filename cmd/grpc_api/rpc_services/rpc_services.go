package rpc_services

import (
	"google.golang.org/grpc"
	"wallet/cmd/grpc_api/rpc_services/internal/leaderboard_server"
	"wallet/cmd/grpc_api/rpc_services/internal/wallet_server"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/pkg/di"
)

func RegisterServices(grpcServer *grpc.Server) {
	usecases := di.GetUsecases()

	wallet.RegisterServiceServer(grpcServer, wallet_server.New(usecases.Wallet()))
	leaderboard.RegisterServiceServer(grpcServer, leaderboard_server.New())
}
