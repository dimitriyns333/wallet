package leaderboard

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"wallet/cmd/grpc_api/clients/base_client"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
)

type Client struct {
	*base_client.BaseClient
	client leaderboard.ServiceClient
}

func New(addr string, creds credentials.TransportCredentials) *Client {
	return &Client{
		BaseClient: base_client.New(addr, creds),
	}
}

func (c *Client) GetClient() leaderboard.ServiceClient {
	if c.client == nil {
		panic("client not started")
	}

	return c.client
}

func (c *Client) Run() error {
	return c.BaseClient.Run(func(conn *grpc.ClientConn) {
		c.client = leaderboard.NewServiceClient(conn)
	})
}

func (c *Client) Shutdown() error {
	return c.BaseClient.Shutdown()
}
