package base_client

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type BaseClient struct {
	conn  *grpc.ClientConn
	addr  string
	creds credentials.TransportCredentials
}

func New(addr string, creds credentials.TransportCredentials) *BaseClient {
	return &BaseClient{
		addr:  addr,
		creds: creds,
	}
}

func (c *BaseClient) Run(setConnCallback func(conn *grpc.ClientConn)) error {
	if c.conn != nil {
		return nil
	}

	conn, err := grpc.Dial(c.addr, grpc.WithTransportCredentials(c.creds))
	if err != nil {
		return err
	}

	c.conn = conn
	setConnCallback(c.conn)

	return nil
}

func (c *BaseClient) Shutdown() error {
	if c.conn == nil {
		return nil
	}

	return c.conn.Close()
}
