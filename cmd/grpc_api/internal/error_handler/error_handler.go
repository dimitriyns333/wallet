package error_handler

import (
	"context"
	"errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log/slog"
	"strings"
	"wallet/pkg/common_errors"
)

func HandleResponseErr(ctx context.Context, err error) *status.Status {
	if err == nil {
		return nil
	}

	var parsedError *common_errors.AppError
	if !errors.As(err, &parsedError) {
		parsedError = common_errors.InternalAppErr
	}

	code := codes.Internal

	// Put the error into response
	if strings.EqualFold(parsedError.Error(), common_errors.ItemNotFoundAppErr.Error()) {
		code = codes.NotFound
	} else if strings.EqualFold(parsedError.Error(), common_errors.BadRequestAppErr.Error()) {
		code = codes.InvalidArgument
	} else if strings.EqualFold(parsedError.Error(), common_errors.Unauthorized.Error()) {
		code = codes.PermissionDenied
	} else {
		slog.ErrorContext(ctx, "[grpc]", "internal error", err)
		parsedError = common_errors.InternalAppErr
	}

	return status.New(code, parsedError.Error())
}
