package main

import (
	"context"
	"errors"
	"github.com/redis/go-redis/v9"
	"google.golang.org/grpc/credentials/insecure"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"time"
	"wallet/cmd/grpc_api"
	"wallet/cmd/grpc_api/clients/leaderboard"
	"wallet/cmd/grpc_api/clients/wallet"
	leaderboardRpc "wallet/cmd/grpc_api/rpc_services/leaderboard"
	walletRpc "wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/cmd/rest_api"
	"wallet/configs"
	"wallet/pkg/di"
	"wallet/pkg/services/leaderboard_service"
	"wallet/pkg/services/wallet_service"
	"wallet/pkg/usecases/leaderboard_usecase"
	"wallet/pkg/usecases/wallet_usecase"
)

func main() {
	quitCh := make(chan os.Signal, 1)
	signal.Notify(quitCh, syscall.SIGINT, syscall.SIGTERM)

	cfg := configs.GetConfig()

	rdb := startRdb(cfg)

	if err := initServices(rdb, cfg.Redis.MaxWatchRetries); err != nil {
		slog.Error("[services]", "error", err)
		panic(err)
	}

	if err := initUsecases(time.Duration(cfg.BaseTimeoutCancelSec) * time.Second); err != nil {
		slog.Error("[services]", "error", err)
		panic(err)
	}

	grpcApi := startGrpcApi(cfg.GrpcPort)

	closeGrpcClientsCallback, err := initGrpcClients(cfg.GrpcClients)
	if err != nil {
		slog.Error("[grpc] init clients", "error", err)
		panic(err)
	}

	restApi := startRestApi(cfg.HttpPort)

	quitSig := <-quitCh
	slog.Info("[shutdown]", "signal", quitSig.String())

	if err = closeGrpcClientsCallback(); err != nil {
		slog.Error("[grpc] init clients", "error", err)
		panic(err)
	}

	grpcApi.Shutdown()

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(cfg.ShutdownTimeoutSec)*time.Second)
	defer cancel()

	if err = restApi.Shutdown(ctx); err != nil {
		slog.ErrorContext(ctx, "[rest api]", "shutdown", err)
	}

	if err = rdb.Close(); err != nil {
		slog.ErrorContext(ctx, "[redis]", "shutdown", err)
	}

	if <-ctx.Done(); true {
		slog.InfoContext(ctx, "[shutdown] on timeout")
	}
}

func startRdb(cfg configs.Settings) *redis.Client {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(cfg.ShutdownTimeoutSec)*time.Second)
	defer cancel()

	rdb := redis.NewClient(&redis.Options{
		Addr:     cfg.Redis.Addr,
		Password: cfg.Redis.Password,
		DB:       cfg.Redis.Db,
	})
	if err := rdb.Ping(ctx).Err(); err != nil {
		slog.ErrorContext(ctx, "[redis]", "ping", err)
		panic(err)
	}

	return rdb
}

func startRestApi(port int) *rest_api.Api {
	restApi := rest_api.New(port)

	restApiCh := restApi.Run()

	go func() {
		for ch := range restApiCh {
			if ch.Err != nil {
				slog.Error("[rest api]", ch.Err)
				panic(ch.Err)
			}

			if ch.Started {
				slog.Info("[rest api] started", "port", port)
				continue
			}

			slog.Info("[rest api] stopped")
			break
		}
	}()

	return restApi
}

func startGrpcApi(port int) *grpc_api.Api {
	grpcApi := grpc_api.NewServer(port)

	restApiCh := grpcApi.Run()

	go func() {
		for ch := range restApiCh {
			if ch.Err != nil {
				slog.Error("[grpc]", ch.Err)
				panic(ch.Err)
			}

			if ch.Started {
				slog.Info("[grpc] started", "port", port)
				continue
			}

			slog.Info("[grpc] stopped")
			break
		}
	}()

	return grpcApi
}

func initServices(rdb *redis.Client, maxWatchRetries int) error {
	return di.InitServices(
		wallet_service.New(rdb, maxWatchRetries),
		leaderboard_service.New(func() leaderboardRpc.ServiceClient {
			return di.GetGrpcClients().Leaderboard()
		}),
	)
}

func initUsecases(contextTimeout time.Duration) error {
	services := di.GetServices()

	return di.InitUsecases(
		wallet_usecase.New(services.Wallet(), contextTimeout, func() walletRpc.ServiceClient {
			return di.GetGrpcClients().Wallet()
		}),
		leaderboard_usecase.New(services.Leaderboard(), contextTimeout),
	)
}

func initGrpcClients(clientsCfg configs.GrpcClients) (close func() error, err error) {
	closeCallbacks := make([]func() error, 0)
	close = func() error {
		var callbackErrors error
		for _, c := range closeCallbacks {
			if callbackErr := c(); callbackErr != nil {
				callbackErrors = errors.Join(callbackErrors, callbackErr)
			}
		}

		return callbackErrors
	}
	defer func() {
		if err == nil {
			return
		}

		err = close()
	}()

	runCallbacks := make([]func() error, 0)
	run := func() error {
		for _, c := range runCallbacks {
			if callbackErr := c(); callbackErr != nil {
				return callbackErr
			}
		}

		return nil
	}

	walletClient := wallet.New(clientsCfg.WalletClient.Addr, insecure.NewCredentials())
	closeCallbacks = append(closeCallbacks, walletClient.Shutdown)
	runCallbacks = append(runCallbacks, walletClient.Run)

	leaderboardClient := leaderboard.New(clientsCfg.LeaderboardClient.Addr, insecure.NewCredentials())
	closeCallbacks = append(closeCallbacks, leaderboardClient.Shutdown)
	runCallbacks = append(runCallbacks, leaderboardClient.Run)

	if err = run(); err != nil {
		return
	}

	err = di.InitGrpcClients(
		walletClient.GetClient(),
		leaderboardClient.GetClient(),
	)

	return
}
