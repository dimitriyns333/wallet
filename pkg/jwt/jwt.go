package jwt

import (
	"context"
	"errors"
	"fmt"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/gin-gonic/gin"
	"github.com/lestrrat-go/jwx/jwt"
	"net/http"
	"strings"
	"wallet/pkg/common_errors"
)

type JWSValidator interface {
	ValidateJWS(jws string) (jwt.Token, error)
	CreateJWSWithClaims(userId string, claims []string) ([]byte, error)
}

const JWTClaimsContextKey = "jwt_claims"
const JWTUserId = "user_id"
const UserIdHeader = "x-user-id"

var (
	errNoAuthHeader      = errors.New("authorization header is missing")
	errInvalidAuthHeader = errors.New("authorization header is malformed")
	errClaimsInvalid     = errors.New("provided claims do not match expected scopes")
	errJwtInvalid        = errors.New("jwt invalid")
)

func NewAuthenticator(v JWSValidator) openapi3filter.AuthenticationFunc {
	return func(ctx context.Context, input *openapi3filter.AuthenticationInput) error {
		if err := authenticate(v, ctx, input); err != nil {
			return fmt.Errorf("%w: %w", common_errors.Unauthorized, err)
		}

		return nil
	}
}

func authenticate(v JWSValidator, ctx context.Context, input *openapi3filter.AuthenticationInput) error {
	if input.SecuritySchemeName != "BearerAuth" {
		return fmt.Errorf("security scheme %s != 'BearerAuth'", input.SecuritySchemeName)
	}

	jws, err := getJWSFromRequest(input.RequestValidationInput.Request)
	if err != nil {
		return fmt.Errorf("getting jws: %w", err)
	}

	token, err := v.ValidateJWS(jws)
	if err != nil {
		return fmt.Errorf("validating JWS: %w", err)
	}

	err = checkTokenClaims(input.Scopes, token)

	if err != nil {
		return fmt.Errorf("token claims don't match: %w", err)
	}

	userId, ok := token.Get(JWTUserId)
	if !ok {
		return errJwtInvalid
	}

	eCtx := (ctx.Value("oapi-codegen/gin-context")).(*gin.Context)
	eCtx.Set(JWTClaimsContextKey, token)
	eCtx.Set(UserIdHeader, userId)

	return nil
}

func getJWSFromRequest(req *http.Request) (string, error) {
	authHdr := req.Header.Get("Authorization")
	// Check for the Authorization header.
	if authHdr == "" {
		return "", errNoAuthHeader
	}
	// We expect a header value of the form "Bearer <token>", with 1 space after
	// Bearer, per spec.
	prefix := "Bearer "
	if !strings.HasPrefix(authHdr, prefix) {
		return "", errInvalidAuthHeader
	}
	return strings.TrimPrefix(authHdr, prefix), nil
}

func getClaimsFromToken(t jwt.Token) ([]string, error) {
	rawPerms, found := t.Get(PermissionsClaim)
	if !found {
		// If the perms aren't found, it means that the token has none, but it has
		// passed signature validation by now, so it's a valid token, so we return
		// the empty list.
		return make([]string, 0), nil
	}

	// rawPerms will be an untyped JSON list, so we need to convert it to
	// a string list.
	rawList, ok := rawPerms.([]interface{})
	if !ok {
		return nil, fmt.Errorf("'%s' claim is unexpected type'", PermissionsClaim)
	}

	claims := make([]string, len(rawList))

	for i, rawClaim := range rawList {
		var ok bool
		claims[i], ok = rawClaim.(string)
		if !ok {
			return nil, fmt.Errorf("%s[%d] is not a string", PermissionsClaim, i)
		}
	}
	return claims, nil
}

func checkTokenClaims(expectedClaims []string, t jwt.Token) error {
	claims, err := getClaimsFromToken(t)
	if err != nil {
		return fmt.Errorf("getting claims from token: %w", err)
	}
	// Put the claims into a map, for quick access.
	claimsMap := make(map[string]bool, len(claims))
	for _, c := range claims {
		claimsMap[c] = true
	}

	for _, e := range expectedClaims {
		if !claimsMap[e] {
			return errClaimsInvalid
		}
	}
	return nil
}
