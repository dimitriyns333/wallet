package leaderboard_domain

import "context"

type Service interface {
	GetLeaderboards(ctx context.Context) (*LeaderboardsResponse, error)
}

type Usecase interface {
	GetLeaderboards(ctx context.Context) (*LeaderboardsResponse, error)
}

type LeaderboardsResponse struct {
	List []float32 `json:"List"`
}
