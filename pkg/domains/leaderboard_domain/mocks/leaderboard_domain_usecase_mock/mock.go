// Code generated by mockery v2.32.4. DO NOT EDIT.

package leaderboard_domain_usecase_mock

import (
	context "context"
	leaderboard_domain "wallet/pkg/domains/leaderboard_domain"

	mock "github.com/stretchr/testify/mock"
)

// Mock is an autogenerated mock type for the Usecase type
type Mock struct {
	mock.Mock
}

type Mock_Expecter struct {
	mock *mock.Mock
}

func (_m *Mock) EXPECT() *Mock_Expecter {
	return &Mock_Expecter{mock: &_m.Mock}
}

// GetLeaderboards provides a mock function with given fields: ctx
func (_m *Mock) GetLeaderboards(ctx context.Context) (*leaderboard_domain.LeaderboardsResponse, error) {
	ret := _m.Called(ctx)

	var r0 *leaderboard_domain.LeaderboardsResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context) (*leaderboard_domain.LeaderboardsResponse, error)); ok {
		return rf(ctx)
	}
	if rf, ok := ret.Get(0).(func(context.Context) *leaderboard_domain.LeaderboardsResponse); ok {
		r0 = rf(ctx)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*leaderboard_domain.LeaderboardsResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Mock_GetLeaderboards_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GetLeaderboards'
type Mock_GetLeaderboards_Call struct {
	*mock.Call
}

// GetLeaderboards is a helper method to define mock.On call
//   - ctx context.Context
func (_e *Mock_Expecter) GetLeaderboards(ctx interface{}) *Mock_GetLeaderboards_Call {
	return &Mock_GetLeaderboards_Call{Call: _e.mock.On("GetLeaderboards", ctx)}
}

func (_c *Mock_GetLeaderboards_Call) Run(run func(ctx context.Context)) *Mock_GetLeaderboards_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context))
	})
	return _c
}

func (_c *Mock_GetLeaderboards_Call) Return(_a0 *leaderboard_domain.LeaderboardsResponse, _a1 error) *Mock_GetLeaderboards_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *Mock_GetLeaderboards_Call) RunAndReturn(run func(context.Context) (*leaderboard_domain.LeaderboardsResponse, error)) *Mock_GetLeaderboards_Call {
	_c.Call.Return(run)
	return _c
}

// NewMock creates a new instance of Mock. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMock(t interface {
	mock.TestingT
	Cleanup(func())
}) *Mock {
	mock := &Mock{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
