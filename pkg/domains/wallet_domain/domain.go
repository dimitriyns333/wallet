package wallet_domain

import (
	"context"
	"wallet/cmd/grpc_api/rpc_services/wallet"
)

type Service interface {
	Deposit(ctx context.Context, userId string, req DepositRequest) (*DepositResponse, error)
	Withdraw(ctx context.Context, userId string, req WithdrawRequest) (*WithdrawResponse, error)
	Balance(ctx context.Context, userId string) (*BalanceResponse, error)
}

type Usecase interface {
	Deposit(ctx context.Context, userId string, req DepositRequest) (*DepositResponse, error)
	Withdraw(ctx context.Context, userId string, req WithdrawRequest) (*WithdrawResponse, error)
	Balance(ctx context.Context, userId string) (*BalanceResponse, error)
	BalanceGrpc(ctx context.Context, req *wallet.GetBalanceRequest) (*wallet.GetBalanceResponse, error)
}

type DepositRequest struct {
	Amount float64 `json:"amount,string"`
}

type WithdrawRequest struct {
	Amount float64 `json:"amount"`
}

type DepositResponse struct {
	Balance float64 `json:"balance"`
}

type WithdrawResponse struct {
	Balance float64 `json:"balance"`
}

type BalanceResponse struct {
	Balance float64 `json:"balance"`
}
