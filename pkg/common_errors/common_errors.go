package common_errors

import (
	"encoding/json"
)

var ItemNotFoundAppErr = &AppError{message: "item not found"}
var BadRequestAppErr = &AppError{message: "bad request"}
var InternalAppErr = &AppError{message: "internal server error"}
var Unauthorized = &AppError{message: "unauthorized"}

type AppError struct {
	message string
}

func (e *AppError) Error() string {
	return e.message
}

func (e *AppError) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Error string `json:"message"`
	}{
		Error: e.message,
	})
}
