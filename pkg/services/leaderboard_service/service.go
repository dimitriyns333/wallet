package leaderboard_service

import (
	"context"
	"io"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
	"wallet/pkg/domains/leaderboard_domain"
)

type service struct {
	leaderboardClient func() leaderboard.ServiceClient
}

func New(leaderboardClient func() leaderboard.ServiceClient) leaderboard_domain.Service {
	return &service{
		leaderboardClient: leaderboardClient,
	}
}

func (s *service) GetLeaderboards(ctx context.Context) (*leaderboard_domain.LeaderboardsResponse, error) {
	stream, err := s.leaderboardClient().ListLeaderboards(ctx, &leaderboard.ListLeaderboardsRequest{})
	if err != nil {
		return nil, err
	}

	resp := &leaderboard_domain.LeaderboardsResponse{}

	for {
		rec, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		resp.List = append(resp.List, rec.List...)
	}

	return resp, nil
}
