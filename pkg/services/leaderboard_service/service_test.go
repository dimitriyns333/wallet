package leaderboard_service

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"os"
	"testing"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
	"wallet/cmd/grpc_api/rpc_services/leaderboard/mocks/leaderboard_service_client_mock"
	"wallet/cmd/grpc_api/rpc_services/leaderboard/mocks/leaderboard_service_list_leaderboards_client_mock"
	"wallet/pkg/domains/leaderboard_domain"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func initMocks() (leaderboard_domain.Service, *leaderboard_service_client_mock.Mock) {
	leaderboardServiceClientMock := new(leaderboard_service_client_mock.Mock)
	leaderboardDomainService := New(func() leaderboard.ServiceClient {
		return leaderboardServiceClientMock
	})

	return leaderboardDomainService, leaderboardServiceClientMock
}

func TestService_GetLeaderboards(t *testing.T) {
	req := leaderboard.ListLeaderboardsRequest{}

	t.Run("success", func(t *testing.T) {
		leaderboardDomainService, leaderboardServiceClientMock := initMocks()
		mockResponse := new(leaderboard_service_list_leaderboards_client_mock.Mock)

		leaderboardServiceClientMock.EXPECT().ListLeaderboards(mock.Anything, &req).Return(mockResponse, nil).Once()
		mockResponse.EXPECT().Recv().Return(&leaderboard.ListLeaderboardsResponse{List: []float32{1, 2}}, nil).Twice()
		mockResponse.EXPECT().Recv().Return(nil, io.EOF).Once()

		resp, err := leaderboardDomainService.GetLeaderboards(context.Background())

		a := assert.New(t)
		a.NoError(err)
		a.NotNil(resp)
		a.Len(resp.List, 4)
		a.Equal(float32(1), resp.List[0])
		a.Equal(float32(2), resp.List[1])
		a.Equal(float32(1), resp.List[2])
		a.Equal(float32(2), resp.List[3])

		leaderboardServiceClientMock.AssertExpectations(t)
		mockResponse.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		customErr := errors.New("unexpected")
		leaderboardDomainService, leaderboardServiceClientMock := initMocks()

		leaderboardServiceClientMock.EXPECT().ListLeaderboards(mock.Anything, &req).Return(nil, customErr).Once()

		resp, err := leaderboardDomainService.GetLeaderboards(context.Background())

		a := assert.New(t)
		a.Error(err)
		a.ErrorIs(customErr, err)
		a.Nil(resp)

		leaderboardServiceClientMock.AssertExpectations(t)
	})
}
