package wallet_service

import (
	"context"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
	"wallet/pkg/common_errors"
	"wallet/pkg/domains/wallet_domain"
)

var maxRetriesErr = errors.New("max retries exceeded")
var zeroOrLessAmountErr = errors.New("amount must be greater than zero")
var insufficientBalanceErr = errors.New("insufficient balance")

type service struct {
	rdb             *redis.Client
	maxWatchRetries int
}

func New(rdb *redis.Client, maxWatchRetries int) wallet_domain.Service {
	return &service{
		rdb:             rdb,
		maxWatchRetries: maxWatchRetries,
	}
}

func (s *service) Deposit(ctx context.Context, userId string,
	req wallet_domain.DepositRequest) (*wallet_domain.DepositResponse, error) {
	if req.Amount <= 0 {
		return nil, fmt.Errorf("%w: %w", common_errors.BadRequestAppErr, zeroOrLessAmountErr)
	}

	var newBalance float64

	txf := func(tx *redis.Tx) error {
		balance, err := tx.Get(ctx, userId).Float64()
		if err != nil && err != redis.Nil {
			return err
		}

		newBalance = balance + req.Amount

		_, err = tx.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
			return pipe.Set(ctx, userId, newBalance, 0).Err()
		})

		return err
	}

	for i := 0; i < s.maxWatchRetries; i++ {
		err := s.rdb.Watch(ctx, txf, userId)
		if err == nil {
			return &wallet_domain.DepositResponse{Balance: newBalance}, err
		}

		if err == redis.TxFailedErr {
			continue
		}

		return nil, err
	}

	return nil, maxRetriesErr
}

func (s *service) Withdraw(ctx context.Context, userId string,
	req wallet_domain.WithdrawRequest) (*wallet_domain.WithdrawResponse, error) {
	if req.Amount <= 0 {
		return nil, fmt.Errorf("%w: %w", common_errors.BadRequestAppErr, zeroOrLessAmountErr)
	}

	var newBalance float64

	txf := func(tx *redis.Tx) error {
		balance, err := tx.Get(ctx, userId).Float64()
		if err != nil {
			if err == redis.Nil {
				return common_errors.ItemNotFoundAppErr
			}

			return err
		}

		newBalance = balance - req.Amount
		if newBalance < 0 {
			return fmt.Errorf("%w: %w", common_errors.BadRequestAppErr, insufficientBalanceErr)
		}

		_, err = tx.TxPipelined(ctx, func(pipe redis.Pipeliner) error {
			return pipe.Set(ctx, userId, newBalance, 0).Err()
		})

		return err
	}

	for i := 0; i < s.maxWatchRetries; i++ {
		err := s.rdb.Watch(ctx, txf, userId)
		if err == nil {
			return &wallet_domain.WithdrawResponse{Balance: newBalance}, err
		}

		if err == redis.TxFailedErr {
			continue
		}

		return nil, err
	}

	return nil, maxRetriesErr
}

func (s *service) Balance(ctx context.Context, userId string) (*wallet_domain.BalanceResponse, error) {
	balance, err := s.rdb.Get(ctx, userId).Float64()
	if err != nil && err != redis.Nil {
		return nil, err
	}

	return &wallet_domain.BalanceResponse{Balance: balance}, nil
}
