package wallet_service

import (
	"context"
	"fmt"
	"github.com/go-redis/redismock/v9"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"wallet/pkg/common_errors"
	"wallet/pkg/domains/wallet_domain"
)

var rdb *redis.Client
var rdbMock redismock.ClientMock
var walletService wallet_domain.Service
var userId string
var amount float64

func TestMain(m *testing.M) {
	rdb, rdbMock = redismock.NewClientMock()

	walletService = New(rdb, 1)

	userId = "1"
	amount = float64(10)

	os.Exit(m.Run())
}

func TestService_Deposit(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		rdbMock.ExpectWatch(userId)
		rdbMock.ExpectGet(userId).SetVal("0")
		rdbMock.ExpectTxPipeline()
		rdbMock.ExpectSet(userId, amount, 0).SetVal(fmt.Sprint(amount))
		rdbMock.ExpectTxPipelineExec()

		resp, err := walletService.Deposit(context.Background(), userId, wallet_domain.DepositRequest{Amount: amount})

		a := assert.New(t)
		a.NotNil(resp)
		a.NoError(err)
		a.Equal(amount, resp.Balance)
	})

	t.Run("error", func(t *testing.T) {
		resp, err := walletService.Deposit(context.Background(), userId, wallet_domain.DepositRequest{Amount: -amount})

		a := assert.New(t)
		a.Nil(resp)
		a.Error(err)
		a.ErrorContains(err, zeroOrLessAmountErr.Error())
		a.ErrorContains(err, common_errors.BadRequestAppErr.Error())
	})
}

func TestService_Withdraw(t *testing.T) {
	t.Run("insufficient_balance_error", func(t *testing.T) {
		rdbMock.ExpectWatch(userId)
		rdbMock.ExpectGet(userId).SetVal("0")

		resp, err := walletService.Withdraw(context.Background(), userId, wallet_domain.WithdrawRequest{Amount: amount})

		a := assert.New(t)
		a.Nil(resp)
		a.Error(err)
		a.ErrorContains(err, insufficientBalanceErr.Error())
		a.ErrorContains(err, common_errors.BadRequestAppErr.Error())
	})

	t.Run("zero_or_less_amount_error", func(t *testing.T) {
		resp, err := walletService.Withdraw(context.Background(), userId, wallet_domain.WithdrawRequest{Amount: -amount})

		a := assert.New(t)
		a.Nil(resp)
		a.Error(err)
		a.ErrorContains(err, zeroOrLessAmountErr.Error())
		a.ErrorContains(err, common_errors.BadRequestAppErr.Error())
	})

	t.Run("success", func(t *testing.T) {
		rdbMock.ClearExpect()

		rdbMock.ExpectWatch(userId)
		rdbMock.ExpectGet(userId).SetVal(fmt.Sprint(amount))
		rdbMock.ExpectTxPipeline()
		rdbMock.ExpectSet(userId, float64(0), 0).SetVal("0")
		rdbMock.ExpectTxPipelineExec()

		resp, err := walletService.Withdraw(context.Background(), userId, wallet_domain.WithdrawRequest{Amount: amount})

		a := assert.New(t)
		a.NotNil(resp)
		a.NoError(err)
		a.Equal(float64(0), resp.Balance)
	})

	t.Run("item_not_found_error", func(t *testing.T) {
		rdbMock.ClearExpect()

		rdbMock.ExpectWatch(userId)
		rdbMock.ExpectGet(userId).RedisNil()
		resp, err := walletService.Withdraw(context.Background(), userId, wallet_domain.WithdrawRequest{Amount: amount})

		a := assert.New(t)
		a.Nil(resp)
		a.Error(err)
		a.ErrorContains(err, common_errors.ItemNotFoundAppErr.Error())
	})
}

func TestService_Balance(t *testing.T) {
	a := assert.New(t)

	rdbMock.ExpectGet(userId).SetVal(fmt.Sprint(amount))
	resp, err := walletService.Balance(context.Background(), userId)

	a.NotNil(resp)
	a.NoError(err)
	a.Equal(amount, resp.Balance)
}
