package di

import (
	"errors"
	"wallet/cmd/grpc_api/rpc_services/leaderboard"
	"wallet/cmd/grpc_api/rpc_services/wallet"
)

var grpcClientsDeps GrpcClients

type GrpcClients interface {
	Wallet() wallet.ServiceClient
	Leaderboard() leaderboard.ServiceClient
}

type grpcClients struct {
	wallet      wallet.ServiceClient
	leaderboard leaderboard.ServiceClient
}

// GetGrpcClients must be called after InitGrpcClients
func GetGrpcClients() GrpcClients {
	if grpcClientsDeps == nil {
		panic("grpcClients must be initialized before")
	}

	return grpcClientsDeps
}

func InitGrpcClients(
	wallet wallet.ServiceClient,
	leaderboard leaderboard.ServiceClient,
) error {
	if grpcClientsDeps != nil {
		return errors.New("grpcClients were initialized before")
	}

	grpcClientsDeps = &grpcClients{
		wallet:      wallet,
		leaderboard: leaderboard,
	}

	return nil
}

func (s *grpcClients) Wallet() wallet.ServiceClient {
	return s.wallet
}

func (s *grpcClients) Leaderboard() leaderboard.ServiceClient {
	return s.leaderboard
}
