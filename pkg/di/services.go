package di

import (
	"errors"
	"wallet/pkg/domains/leaderboard_domain"
	"wallet/pkg/domains/wallet_domain"
)

var servicesDeps Services

type Services interface {
	Wallet() wallet_domain.Service
	Leaderboard() leaderboard_domain.Service
}

type services struct {
	wallet      wallet_domain.Service
	leaderboard leaderboard_domain.Service
}

// GetServices must be called after InitServices
func GetServices() Services {
	if servicesDeps == nil {
		panic("services must be initialized before")
	}

	return servicesDeps
}

func InitServices(
	wallet wallet_domain.Service,
	leaderboard leaderboard_domain.Service,
) error {
	if servicesDeps != nil {
		return errors.New("services were initialized before")
	}

	servicesDeps = &services{
		wallet:      wallet,
		leaderboard: leaderboard,
	}

	return nil
}

func (s *services) Wallet() wallet_domain.Service {
	return s.wallet
}

func (s *services) Leaderboard() leaderboard_domain.Service {
	return s.leaderboard
}
