package di

import (
	"errors"
	"wallet/pkg/domains/leaderboard_domain"
	"wallet/pkg/domains/wallet_domain"
)

var usecasesDeps Usecases

type Usecases interface {
	Wallet() wallet_domain.Usecase
	Leaderboard() leaderboard_domain.Usecase
}

type usecases struct {
	wallet      wallet_domain.Usecase
	leaderboard leaderboard_domain.Usecase
}

// GetUsecases must be called after InitUsecases
func GetUsecases() Usecases {
	if usecasesDeps == nil {
		panic("usecases must be initialized before")
	}

	return usecasesDeps
}

func InitUsecases(
	wallet wallet_domain.Usecase,
	leaderboard leaderboard_domain.Usecase,
) error {
	if usecasesDeps != nil {
		return errors.New("usecases were initialized before")
	}

	usecasesDeps = &usecases{
		wallet:      wallet,
		leaderboard: leaderboard,
	}

	return nil
}

func (s *usecases) Wallet() wallet_domain.Usecase {
	return s.wallet
}

func (s *usecases) Leaderboard() leaderboard_domain.Usecase {
	return s.leaderboard
}
