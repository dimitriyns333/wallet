package wallet_usecase

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"os"
	"testing"
	"time"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/cmd/grpc_api/rpc_services/wallet/mocks/wallet_service_client_mock"
	"wallet/pkg/domains/wallet_domain"
	"wallet/pkg/domains/wallet_domain/mocks/wallet_domain_service_mock"
)

var userId string
var amount float64

func TestMain(m *testing.M) {
	userId = "1"
	amount = 10

	os.Exit(m.Run())
}

func initMocks() (*wallet_domain_service_mock.Mock, *wallet_service_client_mock.Mock, wallet_domain.Usecase) {
	walletServiceMock := new(wallet_domain_service_mock.Mock)
	walletClientMock := new(wallet_service_client_mock.Mock)
	walletUsecase := New(walletServiceMock, time.Second, func() wallet.ServiceClient {
		return walletClientMock
	})
	return walletServiceMock, walletClientMock, walletUsecase
}

func TestUsecase_Deposit(t *testing.T) {
	req := wallet_domain.DepositRequest{Amount: amount}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.DepositResponse{Balance: amount}

		walletServiceMock, _, walletUsecase := initMocks()
		walletServiceMock.EXPECT().Deposit(mock.Anything, userId, req).Return(mockResponse, nil).Once()

		resp, err := walletUsecase.Deposit(context.Background(), userId, req)

		a := assert.New(t)
		a.NoError(err)
		a.NotNil(resp)
		walletServiceMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletServiceMock, _, walletUsecase := initMocks()

		walletServiceMock.EXPECT().Deposit(mock.Anything, userId, req).
			Return(nil, errors.New("unexpected")).Once()

		resp, err := walletUsecase.Deposit(context.Background(), userId, req)

		a := assert.New(t)
		a.Error(err)
		a.Nil(resp)
		walletServiceMock.AssertExpectations(t)
	})
}

func TestUsecase_Withdraw(t *testing.T) {
	req := wallet_domain.WithdrawRequest{Amount: amount}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.WithdrawResponse{Balance: 0}

		walletServiceMock, _, walletUsecase := initMocks()

		walletServiceMock.EXPECT().Withdraw(mock.Anything, userId, req).Return(mockResponse, nil).Once()

		resp, err := walletUsecase.Withdraw(context.Background(), userId, req)

		a := assert.New(t)
		a.NoError(err)
		a.NotNil(resp)
		walletServiceMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletServiceMock, _, walletUsecase := initMocks()

		walletServiceMock.EXPECT().Withdraw(mock.Anything, userId, req).
			Return(nil, errors.New("unexpected")).Once()

		resp, err := walletUsecase.Withdraw(context.Background(), userId, req)

		a := assert.New(t)
		a.Error(err)
		a.Nil(resp)
		walletServiceMock.AssertExpectations(t)
	})
}

func TestUsecase_Balance(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet_domain.BalanceResponse{Balance: amount}

		walletServiceMock, _, walletUsecase := initMocks()

		walletServiceMock.EXPECT().Balance(mock.Anything, userId).Return(mockResponse, nil).Once()

		resp, err := walletUsecase.Balance(context.Background(), userId)

		a := assert.New(t)
		a.NoError(err)
		a.NotNil(resp)
		walletServiceMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		walletServiceMock, _, walletUsecase := initMocks()

		walletServiceMock.EXPECT().Balance(mock.Anything, userId).
			Return(nil, errors.New("unexpected")).Once()

		resp, err := walletUsecase.Balance(context.Background(), userId)

		a := assert.New(t)
		a.Error(err)
		a.Nil(resp)
		walletServiceMock.AssertExpectations(t)
	})
}

func TestUsecase_BalanceGrpc(t *testing.T) {
	req := wallet.GetBalanceRequest{UserId: userId}

	t.Run("success", func(t *testing.T) {
		mockResponse := &wallet.GetBalanceResponse{Balance: 0}

		_, walletClientMock, walletUsecase := initMocks()

		walletClientMock.EXPECT().GetBalance(mock.Anything, &req).Return(mockResponse, nil).Once()

		resp, err := walletUsecase.BalanceGrpc(context.Background(), &req)

		a := assert.New(t)
		a.NoError(err)
		a.NotNil(resp)
		walletClientMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		_, walletClientMock, walletUsecase := initMocks()

		walletClientMock.EXPECT().GetBalance(mock.Anything, &req).
			Return(nil, errors.New("unexpected")).Once()

		resp, err := walletUsecase.BalanceGrpc(context.Background(), &req)

		a := assert.New(t)
		a.Error(err)
		a.Nil(resp)
		walletClientMock.AssertExpectations(t)
	})
}
