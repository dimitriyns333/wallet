package wallet_usecase

import (
	"context"
	"time"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/pkg/domains/wallet_domain"
)

type usecase struct {
	walletService  wallet_domain.Service
	contextTimeout time.Duration
	walletClient   func() wallet.ServiceClient
}

func New(walletService wallet_domain.Service, contextTimeout time.Duration,
	walletClient func() wallet.ServiceClient) wallet_domain.Usecase {
	return &usecase{
		walletService:  walletService,
		contextTimeout: contextTimeout,
		walletClient:   walletClient,
	}
}

func (u *usecase) Deposit(c context.Context, userId string, req wallet_domain.DepositRequest) (*wallet_domain.DepositResponse, error) {
	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	return u.walletService.Deposit(ctx, userId, req)
}

func (u *usecase) Withdraw(c context.Context, userId string, req wallet_domain.WithdrawRequest) (*wallet_domain.WithdrawResponse, error) {
	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	return u.walletService.Withdraw(ctx, userId, req)
}

func (u *usecase) Balance(c context.Context, userId string) (*wallet_domain.BalanceResponse, error) {
	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	return u.walletService.Balance(ctx, userId)
}

func (u *usecase) BalanceGrpc(c context.Context, req *wallet.GetBalanceRequest) (*wallet.GetBalanceResponse, error) {
	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	return u.walletClient().GetBalance(ctx, req)
}
