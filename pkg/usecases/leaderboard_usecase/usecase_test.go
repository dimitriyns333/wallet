package leaderboard_usecase

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"os"
	"testing"
	"time"
	"wallet/pkg/domains/leaderboard_domain"
	"wallet/pkg/domains/leaderboard_domain/mocks/leaderboard_domain_service_mock"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func initMocks() (leaderboard_domain.Usecase, *leaderboard_domain_service_mock.Mock) {
	leaderboardServiceMock := new(leaderboard_domain_service_mock.Mock)
	leaderboardUsecase := New(leaderboardServiceMock, time.Second)
	return leaderboardUsecase, leaderboardServiceMock
}

func TestUsecase_GetLeaderboards(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		mockResponse := &leaderboard_domain.LeaderboardsResponse{List: []float32{1, 2}}

		leaderboardUsecase, leaderboardServiceMock := initMocks()

		leaderboardServiceMock.EXPECT().GetLeaderboards(mock.Anything).Return(mockResponse, nil).Once()

		resp, err := leaderboardUsecase.GetLeaderboards(context.Background())

		a := assert.New(t)

		a.NoError(err)
		a.NotNil(resp)
		a.Equal(float32(1), resp.List[0])
		a.Equal(float32(2), resp.List[1])

		leaderboardServiceMock.AssertExpectations(t)
	})

	t.Run("error", func(t *testing.T) {
		customErr := errors.New("unexpected")
		leaderboardUsecase, leaderboardServiceMock := initMocks()

		leaderboardServiceMock.EXPECT().GetLeaderboards(mock.Anything).Return(nil, customErr).Once()

		resp, err := leaderboardUsecase.GetLeaderboards(context.Background())

		a := assert.New(t)

		a.Error(err)
		a.ErrorIs(customErr, err)
		a.Nil(resp)

		leaderboardServiceMock.AssertExpectations(t)
	})
}
