package leaderboard_usecase

import (
	"context"
	"time"
	"wallet/cmd/grpc_api/rpc_services/wallet"
	"wallet/pkg/domains/leaderboard_domain"
)

type usecase struct {
	leaderboardService leaderboard_domain.Service
	contextTimeout     time.Duration
	walletClient       wallet.ServiceClient
}

func New(leaderboardService leaderboard_domain.Service, contextTimeout time.Duration) leaderboard_domain.Usecase {
	return &usecase{
		leaderboardService: leaderboardService,
		contextTimeout:     contextTimeout,
	}
}

func (u *usecase) GetLeaderboards(c context.Context) (*leaderboard_domain.LeaderboardsResponse, error) {
	ctx, cancel := context.WithTimeout(c, u.contextTimeout)
	defer cancel()

	return u.leaderboardService.GetLeaderboards(ctx)
}
